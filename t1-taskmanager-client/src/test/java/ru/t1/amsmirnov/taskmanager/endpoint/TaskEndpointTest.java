package ru.t1.amsmirnov.taskmanager.endpoint;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.amsmirnov.taskmanager.api.endpoint.IAuthEndpoint;
import ru.t1.amsmirnov.taskmanager.api.endpoint.IProjectEndpoint;
import ru.t1.amsmirnov.taskmanager.api.endpoint.ITaskEndpoint;
import ru.t1.amsmirnov.taskmanager.dto.request.project.ProjectCreateRequest;
import ru.t1.amsmirnov.taskmanager.dto.request.task.*;
import ru.t1.amsmirnov.taskmanager.dto.request.user.UserLoginRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.project.ProjectCreateResponse;
import ru.t1.amsmirnov.taskmanager.dto.response.task.*;
import ru.t1.amsmirnov.taskmanager.dto.response.user.UserLoginResponse;
import ru.t1.amsmirnov.taskmanager.enumerated.Status;
import ru.t1.amsmirnov.taskmanager.enumerated.TaskSort;
import ru.t1.amsmirnov.taskmanager.marker.SoapCategory;
import ru.t1.amsmirnov.taskmanager.model.Project;
import ru.t1.amsmirnov.taskmanager.model.Task;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@Category(value = SoapCategory.class)
public class TaskEndpointTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static final String NONE_STR = "---NONE---";

    @NotNull
    private final static ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance();

    @NotNull
    private final static IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance();

    @NotNull
    private final static IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    @NotNull
    private final static String login = "test";

    @NotNull
    private final static String password = "test";

    @NotNull
    private final List<Task> tasks = new ArrayList<>();

    @NotNull
    private static String token;

    @Nullable
    private static Project project;

    @BeforeClass
    public static void login() {
        @NotNull final UserLoginRequest request = new UserLoginRequest(login, password);
        @NotNull final UserLoginResponse response = authEndpoint.login(request);
        token = response.getToken();

        @NotNull final ProjectCreateRequest createProjectRequest = new ProjectCreateRequest(token, "TEST Project", "Test description");
        @NotNull final ProjectCreateResponse projectResponse = projectEndpoint.createProject(createProjectRequest);
        project = projectResponse.getProject();
    }

    @Before
    public void initData() {
        @NotNull TaskCreateRequest request;
        @NotNull TaskCreateResponse response;
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            request = new TaskCreateRequest(token, "TEST Task " + i, "Test description " + i);
            response = taskEndpoint.createTask(request);
            tasks.add(response.getTask());
        }
    }

    @After
    public void clearData() {
        taskEndpoint.removeAllTasks(new TaskClearRequest(token));
    }

    @Test
    public void testTaskBindToProject() {
        @NotNull TaskCreateRequest createRequest = new TaskCreateRequest(token, "Test bind", "Description");
        @NotNull TaskCreateResponse createResponse = taskEndpoint.createTask(createRequest);

        @NotNull TaskBindToProjectRequest bindRequest = new TaskBindToProjectRequest(token, createResponse.getTask().getId(), project.getId());
        @NotNull TaskBindToProjectResponse bindResponse = taskEndpoint.bindTaskToProject(bindRequest);
        assertTrue(bindResponse.isSuccess());

        @NotNull TaskShowByIdRequest findRequest = new TaskShowByIdRequest(token, createResponse.getTask().getId());
        @NotNull final TaskShowByIdResponse response = taskEndpoint.findOneTaskById(findRequest);
        assertEquals(project.getId(), response.getTask().getProjectId());

        bindRequest = new TaskBindToProjectRequest(token, NONE_STR, project.getId());
        bindResponse = taskEndpoint.bindTaskToProject(bindRequest);
        assertFalse(bindResponse.isSuccess());
        assertNotNull(bindResponse.getMessage());

        bindRequest = new TaskBindToProjectRequest(token, createResponse.getTask().getId(), NONE_STR);
        bindResponse = taskEndpoint.bindTaskToProject(bindRequest);
        assertFalse(bindResponse.isSuccess());
        assertNotNull(bindResponse.getMessage());

        bindRequest = new TaskBindToProjectRequest(NONE_STR, createResponse.getTask().getId(), project.getId());
        bindResponse = taskEndpoint.bindTaskToProject(bindRequest);
        assertFalse(bindResponse.isSuccess());
        assertNotNull(bindResponse.getMessage());
    }

    @Test
    public void testTaskChangeStatusById() {
        @NotNull TaskChangeStatusByIdRequest request;
        @NotNull TaskChangeStatusByIdResponse response;
        for (@NotNull final Task task : tasks) {
            request = new TaskChangeStatusByIdRequest(token, task.getId(), Status.IN_PROGRESS);
            response = taskEndpoint.changeTaskStatusById(request);
            assertTrue(response.isSuccess());
        }
        request = new TaskChangeStatusByIdRequest(NONE_STR, NONE_STR, Status.IN_PROGRESS);
        response = taskEndpoint.changeTaskStatusById(request);
        assertFalse(response.isSuccess());
        assertNotNull(response.getMessage());

        request = new TaskChangeStatusByIdRequest(token, NONE_STR, Status.IN_PROGRESS);
        response = taskEndpoint.changeTaskStatusById(request);
        assertFalse(response.isSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testTaskRemoveAll() {
        @NotNull TaskShowByIdRequest findRequest = new TaskShowByIdRequest(token, tasks.get(0).getId());
        @NotNull TaskShowByIdResponse findResponse = taskEndpoint.findOneTaskById(findRequest);
        assertNotNull(findResponse.getTask());
        assertEquals(tasks.get(0), findResponse.getTask());

        @NotNull TaskClearRequest clearRequest = new TaskClearRequest(token);
        @NotNull TaskClearResponse clearResponse = taskEndpoint.removeAllTasks(clearRequest);
        assertTrue(clearResponse.isSuccess());

        findResponse = taskEndpoint.findOneTaskById(findRequest);
        assertNull(findResponse.getTask());

        clearRequest = new TaskClearRequest(NONE_STR);
        clearResponse = taskEndpoint.removeAllTasks(clearRequest);
        assertFalse(clearResponse.isSuccess());
        assertNotNull(clearResponse.getMessage());
    }

    @Test
    public void testTaskCompleteById() {
        @NotNull TaskCompleteByIdRequest request;
        @NotNull TaskCompleteByIdResponse response;
        for (@NotNull final Task task : tasks) {
            request = new TaskCompleteByIdRequest(token, task.getId());
            response = taskEndpoint.completeTaskById(request);
            assertEquals(Status.COMPLETED, response.getTask().getStatus());
            assertTrue(response.isSuccess());
        }
        request = new TaskCompleteByIdRequest(token, NONE_STR);
        response = taskEndpoint.completeTaskById(request);
        assertFalse(response.isSuccess());
        assertNotNull(response.getMessage());

        request = new TaskCompleteByIdRequest(token, "");
        response = taskEndpoint.completeTaskById(request);
        assertFalse(response.isSuccess());
        assertNotNull(response.getMessage());

        request = new TaskCompleteByIdRequest("", "123");
        response = taskEndpoint.completeTaskById(request);
        assertFalse(response.isSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testTaskCreate() {
        @NotNull final String name = "Test create";
        @NotNull final String description = "description";
        @NotNull TaskCreateRequest request = new TaskCreateRequest(token, name, description);
        @NotNull TaskCreateResponse response = taskEndpoint.createTask(request);
        assertEquals(name, response.getTask().getName());
        assertEquals(description, response.getTask().getDescription());
        assertEquals(Status.NOT_STARTED, response.getTask().getStatus());

        request = new TaskCreateRequest(NONE_STR, "name", "description");
        response = taskEndpoint.createTask(request);
        assertFalse(response.isSuccess());
        assertNotNull(response.getMessage());

        request = new TaskCreateRequest(token, "", "description");
        response = taskEndpoint.createTask(request);
        assertFalse(response.isSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testTaskFindAll() {
        @NotNull TaskListRequest request = new TaskListRequest(token, TaskSort.BY_DEFAULT);
        @NotNull TaskListResponse response = taskEndpoint.findAllTasks(request);
        assertEquals(tasks, response.getTasks());

        request = new TaskListRequest(NONE_STR, TaskSort.BY_DEFAULT);
        response = taskEndpoint.findAllTasks(request);
        assertFalse(response.isSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testTaskFindById() {
        @NotNull TaskShowByIdRequest request;
        @NotNull TaskShowByIdResponse response;
        for (@NotNull final Task task : tasks) {
            request = new TaskShowByIdRequest(token, task.getId());
            response = taskEndpoint.findOneTaskById(request);
            assertEquals(task, response.getTask());
        }
        request = new TaskShowByIdRequest(NONE_STR, NONE_STR);
        response = taskEndpoint.findOneTaskById(request);
        assertFalse(response.isSuccess());
        assertNotNull(response.getMessage());

        request = new TaskShowByIdRequest(token, NONE_STR);
        response = taskEndpoint.findOneTaskById(request);
        assertFalse(response.isSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testTaskFindByProjectId() {
        @NotNull final TaskCreateRequest createRequest = new TaskCreateRequest(token, "Test find by project", "Test description");
        @NotNull final TaskCreateResponse createResponse = taskEndpoint.createTask(createRequest);
        @NotNull TaskShowByProjectIdRequest findRequest = new TaskShowByProjectIdRequest(token, project.getId());
        @NotNull TaskShowByProjectIdResponse findResponse = taskEndpoint.findAllTasksByProjectId(findRequest);
        assertNull(findResponse.getTasks());

        @NotNull final TaskBindToProjectRequest bindRequest = new TaskBindToProjectRequest(token, createResponse.getTask().getId(), project.getId());
        taskEndpoint.bindTaskToProject(bindRequest);
        findResponse = taskEndpoint.findAllTasksByProjectId(findRequest);
        assertNotNull(findResponse.getTasks());

        findRequest = new TaskShowByProjectIdRequest(NONE_STR, project.getId());
        findResponse = taskEndpoint.findAllTasksByProjectId(findRequest);
        assertFalse(findResponse.isSuccess());
        assertNotNull(findResponse.getMessage());

        findRequest = new TaskShowByProjectIdRequest(token, NONE_STR);
        findResponse = taskEndpoint.findAllTasksByProjectId(findRequest);
        assertTrue(findResponse.isSuccess());
        assertNull(findResponse.getTasks());
    }

    @Test
    public void testTaskStartById() {
        @NotNull TaskStartByIdRequest request;
        @NotNull TaskStartByIdResponse response;
        for (@NotNull final Task task : tasks) {
            request = new TaskStartByIdRequest(token, task.getId());
            response = taskEndpoint.startTaskById(request);
            assertEquals(Status.IN_PROGRESS, response.getTask().getStatus());
        }
        request = new TaskStartByIdRequest(NONE_STR, NONE_STR);
        response = taskEndpoint.startTaskById(request);
        assertFalse(response.isSuccess());
        assertNotNull(response.getMessage());

        request = new TaskStartByIdRequest(token, NONE_STR);
        response = taskEndpoint.startTaskById(request);
        assertFalse(response.isSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testTaskUnbindFromProject() {
        @NotNull final TaskCreateRequest createRequest = new TaskCreateRequest(token, "TEST unbind", "Test description");
        @NotNull final TaskCreateResponse createResponse = taskEndpoint.createTask(createRequest);
        @NotNull final TaskBindToProjectRequest bindRequest = new TaskBindToProjectRequest(token, createResponse.getTask().getId(), project.getId());
        @NotNull final TaskBindToProjectResponse bindResponse = taskEndpoint.bindTaskToProject(bindRequest);
        assertTrue(bindResponse.isSuccess());

        @NotNull TaskUnbindFromProjectRequest unbindRequest = new TaskUnbindFromProjectRequest(token, createResponse.getTask().getId(), project.getId());
        @NotNull TaskUnbindFromProjectResponse unbindResponse = taskEndpoint.unbindTaskFromProject(unbindRequest);
        assertTrue(unbindResponse.isSuccess());

        @NotNull final TaskShowByIdRequest findRequest = new TaskShowByIdRequest(token, createResponse.getTask().getId());
        @NotNull final TaskShowByIdResponse findResponse = taskEndpoint.findOneTaskById(findRequest);
        assertNull(findResponse.getTask().getProjectId());

        unbindRequest = new TaskUnbindFromProjectRequest(NONE_STR, NONE_STR, NONE_STR);
        unbindResponse = taskEndpoint.unbindTaskFromProject(unbindRequest);
        assertFalse(unbindResponse.isSuccess());
        assertNotNull(unbindResponse.getMessage());

        unbindRequest = new TaskUnbindFromProjectRequest(token, "", NONE_STR);
        unbindResponse = taskEndpoint.unbindTaskFromProject(unbindRequest);
        assertFalse(unbindResponse.isSuccess());
        assertNotNull(unbindResponse.getMessage());

        unbindRequest = new TaskUnbindFromProjectRequest(token, NONE_STR, "");
        unbindResponse = taskEndpoint.unbindTaskFromProject(unbindRequest);
        assertFalse(unbindResponse.isSuccess());
        assertNotNull(unbindResponse.getMessage());
    }

    @Test
    public void testTaskUpdateById() {
        @NotNull TaskUpdateByIdRequest request;
        @NotNull TaskUpdateByIdResponse response;
        for (final Task task : tasks) {
            @NotNull final String name = task.getName() + " TEST";
            @NotNull final String description = task.getDescription() + " TEST";
            request = new TaskUpdateByIdRequest(token, task.getId(), name, description);
            response = taskEndpoint.updateTaskById(request);
            assertEquals(name, response.getTask().getName());
            assertEquals(description, response.getTask().getDescription());
        }
        request = new TaskUpdateByIdRequest(NONE_STR, NONE_STR, NONE_STR, NONE_STR);
        response = taskEndpoint.updateTaskById(request);
        assertFalse(response.isSuccess());
        assertNotNull(response.getMessage());

        request = new TaskUpdateByIdRequest(token, "", NONE_STR, NONE_STR);
        response = taskEndpoint.updateTaskById(request);
        assertFalse(response.isSuccess());
        assertNotNull(response.getMessage());

        request = new TaskUpdateByIdRequest(token, tasks.get(0).getId(), "", NONE_STR);
        response = taskEndpoint.updateTaskById(request);
        assertFalse(response.isSuccess());
        assertNotNull(response.getMessage());
    }

}
