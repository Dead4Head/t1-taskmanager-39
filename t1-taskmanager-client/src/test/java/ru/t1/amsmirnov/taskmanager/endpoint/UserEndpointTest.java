package ru.t1.amsmirnov.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.amsmirnov.taskmanager.api.endpoint.IAuthEndpoint;
import ru.t1.amsmirnov.taskmanager.api.endpoint.IUserEndpoint;
import ru.t1.amsmirnov.taskmanager.dto.request.user.*;
import ru.t1.amsmirnov.taskmanager.dto.response.user.*;
import ru.t1.amsmirnov.taskmanager.marker.SoapCategory;
import ru.t1.amsmirnov.taskmanager.model.User;

import static org.junit.Assert.*;

@Category(value = SoapCategory.class)
public class UserEndpointTest {

    @NotNull
    private static final String NONE_STR = "---NONE---";

    @NotNull
    private static final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    @NotNull
    private static final IUserEndpoint userEndpoint = IUserEndpoint.newInstance();

    @NotNull
    private static final String ADMIN_LOGIN = "admin";

    @NotNull
    private static final String ADMIN_PASSWORD = "admin";

    @NotNull
    private static final String TEST_LOGIN = "new_test";

    @NotNull
    private static final String TEST_PASSWORD = "new_test";

    @Nullable
    private static String token;

    @Nullable
    private static String adminToken;

    @Nullable
    private static User user;

    @BeforeClass
    public static void login() {
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest(ADMIN_LOGIN, ADMIN_PASSWORD);
        @NotNull final UserLoginResponse adminResponse = authEndpoint.login(loginRequest);
        adminToken = adminResponse.getToken();
    }

    @Before
    public void createUser() {
        @NotNull final UserRegistryRequest registryRequest = new UserRegistryRequest(TEST_LOGIN, TEST_PASSWORD, "test@email.ru");
        @NotNull final UserRegistryResponse registryResponse = userEndpoint.registry(registryRequest);
        user = registryResponse.getUser();
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest(TEST_LOGIN, TEST_PASSWORD);
        @NotNull final UserLoginResponse loginResponse = authEndpoint.login(loginRequest);
        token = loginResponse.getToken();
    }

    @After
    public void removeUser() {
        UserRemoveResponse response = userEndpoint.removeByLogin(new UserRemoveRequest(adminToken, TEST_LOGIN));
    }

    @Test
    public void testUserChangePassword() {
        @NotNull final String newPassword = "new password";
        @NotNull UserChangePasswordRequest request = new UserChangePasswordRequest(token, newPassword);
        @NotNull UserChangePasswordResponse response = userEndpoint.changePassword(request);
        assertNotEquals(user.getPasswordHash(), response.getUser().getPasswordHash());

        request = new UserChangePasswordRequest(token, "");
        response = userEndpoint.changePassword(request);
        assertFalse(response.isSuccess());
        assertNotNull(response.getMessage());

        request = new UserChangePasswordRequest(NONE_STR, "newPassword");
        response = userEndpoint.changePassword(request);
        assertFalse(response.isSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testUserLock() {
        @NotNull UserProfileRequest profileRequest = new UserProfileRequest(token);
        @NotNull UserProfileResponse profileResponse = userEndpoint.viewProfile(profileRequest);
        assertFalse(profileResponse.getUser().isLocked());

        @NotNull UserLockRequest lockRequest = new UserLockRequest(adminToken, TEST_LOGIN);
        @NotNull UserLockResponse lockResponse = userEndpoint.lockUserByLogin(lockRequest);
        assertTrue(lockResponse.isSuccess());

        profileResponse = userEndpoint.viewProfile(profileRequest);
        assertTrue(profileResponse.getUser().isLocked());

        lockRequest = new UserLockRequest(adminToken, "");
        lockResponse = userEndpoint.lockUserByLogin(lockRequest);
        assertFalse(lockResponse.isSuccess());
        assertNotNull(lockResponse.getMessage());

        lockRequest = new UserLockRequest(adminToken, NONE_STR);
        lockResponse = userEndpoint.lockUserByLogin(lockRequest);
        assertFalse(lockResponse.isSuccess());
        assertNotNull(lockResponse.getMessage());
    }

    @Test
    public void testUserProfile() {
        @NotNull UserProfileRequest request = new UserProfileRequest(token);
        @NotNull UserProfileResponse response = userEndpoint.viewProfile(request);
        assertEquals(user, response.getUser());

        request = new UserProfileRequest(NONE_STR);
        response = userEndpoint.viewProfile(request);
        assertFalse(response.isSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testUserRegistry() {
        @NotNull final String newUserLogin = "new_login";
        @NotNull final String newUserPassword = "new_password";
        @NotNull UserRegistryRequest request = new UserRegistryRequest(newUserLogin, newUserPassword, newUserLogin + "@mail.ru");
        @NotNull UserRegistryResponse response = userEndpoint.registry(request);
        assertEquals(newUserLogin, response.getUser().getLogin());

        request = new UserRegistryRequest("", newUserPassword, newUserLogin + "@mail.ru");
        response = userEndpoint.registry(request);
        assertFalse(response.isSuccess());
        assertNotNull(response.getMessage());

        request = new UserRegistryRequest(newUserLogin, "", newUserLogin + "@mail.ru");
        response = userEndpoint.registry(request);
        assertFalse(response.isSuccess());
        assertNotNull(response.getMessage());

        request = new UserRegistryRequest(newUserLogin, newUserPassword, newUserLogin + "@mail.ru");
        response = userEndpoint.registry(request);
        assertFalse(response.isSuccess());
        assertNotNull(response.getMessage());

        request = new UserRegistryRequest(newUserLogin, NONE_STR, newUserLogin + "@mail.ru");
        response = userEndpoint.registry(request);
        assertFalse(response.isSuccess());
        assertNotNull(response.getMessage());

        userEndpoint.removeByLogin(new UserRemoveRequest(adminToken, newUserLogin));
    }

    @Test
    public void testUserRemove() {
        @NotNull UserRemoveRequest request = new UserRemoveRequest(adminToken, TEST_LOGIN);
        @NotNull UserRemoveResponse response = userEndpoint.removeByLogin(request);
        assertTrue(response.isSuccess());
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest(TEST_LOGIN, TEST_PASSWORD);
        @NotNull final UserLoginResponse loginResponse = authEndpoint.login(loginRequest);
        assertFalse(loginResponse.isSuccess());
        assertNull(loginResponse.getToken());

        request = new UserRemoveRequest(token, TEST_LOGIN);
        response = userEndpoint.removeByLogin(request);
        assertFalse(response.isSuccess());
        assertNotNull(response.getMessage());

        request = new UserRemoveRequest(adminToken, "");
        response = userEndpoint.removeByLogin(request);
        assertFalse(response.isSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testUserUnlock() {
        @NotNull UserProfileRequest profileRequest = new UserProfileRequest(token);
        @NotNull UserProfileResponse profileResponse;
        profileResponse = userEndpoint.viewProfile(profileRequest);
        assertFalse(profileResponse.getUser().isLocked());

        @NotNull final UserLockRequest lockRequest = new UserLockRequest(adminToken, TEST_LOGIN);
        @NotNull final UserLockResponse lockResponse = userEndpoint.lockUserByLogin(lockRequest);
        assertTrue(lockResponse.isSuccess());
        profileResponse = userEndpoint.viewProfile(profileRequest);
        assertTrue(profileResponse.getUser().isLocked());

        @NotNull UserUnlockRequest unlockRequest = new UserUnlockRequest(adminToken, TEST_LOGIN);
        @NotNull UserUnlockResponse unlockResponse = userEndpoint.unlockUserByLogin(unlockRequest);

        profileResponse = userEndpoint.viewProfile(new UserProfileRequest(token));
        assertTrue(unlockResponse.isSuccess());
        ;
        assertFalse(profileResponse.getUser().isLocked());

        unlockRequest = new UserUnlockRequest(NONE_STR, TEST_LOGIN);
        unlockResponse = userEndpoint.unlockUserByLogin(unlockRequest);
        assertFalse(unlockResponse.isSuccess());
        assertNotNull(unlockResponse.getMessage());

        unlockRequest = new UserUnlockRequest(adminToken, "");
        unlockResponse = userEndpoint.unlockUserByLogin(unlockRequest);
        assertFalse(unlockResponse.isSuccess());
        assertNotNull(unlockResponse.getMessage());
    }

    @Test
    public void testUserUpdate() {
        @NotNull UserUpdateRequest request = new UserUpdateRequest(token, NONE_STR, NONE_STR, NONE_STR);
        @NotNull UserUpdateResponse response = userEndpoint.updateUserById(request);
        assertTrue(response.isSuccess());

        @NotNull final UserProfileRequest profileRequest = new UserProfileRequest(token);
        @NotNull final UserProfileResponse profileResponse = userEndpoint.viewProfile(profileRequest);
        boolean check = NONE_STR.equals(profileResponse.getUser().getFirstName())
                && NONE_STR.equals(profileResponse.getUser().getMiddleName())
                && NONE_STR.equals(profileResponse.getUser().getLastName());
        assertTrue(check);

        request = new UserUpdateRequest(NONE_STR, NONE_STR, NONE_STR, NONE_STR);
        response = userEndpoint.updateUserById(request);
        assertFalse(response.isSuccess());
        assertNotNull(response.getMessage());
    }

}
