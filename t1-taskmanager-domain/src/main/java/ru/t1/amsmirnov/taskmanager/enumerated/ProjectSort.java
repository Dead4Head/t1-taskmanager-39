package ru.t1.amsmirnov.taskmanager.enumerated;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.comparator.CreatedComparator;
import ru.t1.amsmirnov.taskmanager.comparator.NameComparator;
import ru.t1.amsmirnov.taskmanager.comparator.StatusComparator;
import ru.t1.amsmirnov.taskmanager.model.Project;

import java.util.Comparator;

public enum ProjectSort {

    BY_NAME("TaskSort by name", NameComparator.INSTANCE::compare),
    BY_STATUS("TaskSort by status", StatusComparator.INSTANCE::compare),
    BY_CREATED("TaskSort by created", CreatedComparator.INSTANCE::compare),
    BY_DEFAULT("Default sort", CreatedComparator.INSTANCE::compare);

    @NotNull
    private final String name;

    @Nullable
    private final Comparator<Project> comparator;

    @NotNull
    public static ProjectSort toSort(@Nullable final String value) {
        for (final ProjectSort sort : values()) {
            if (sort.name().equals(value) || sort.name.equals(value)) return sort;
        }
        return BY_DEFAULT;
    }

    ProjectSort(@NotNull final String name, @Nullable final Comparator<Project> comparator) {
        this.name = name;
        this.comparator = comparator;
    }

    @NotNull
    public String getName() {
        return name;
    }

    @Nullable
    public Comparator<Project> getComparator() {
        return comparator;
    }

}
