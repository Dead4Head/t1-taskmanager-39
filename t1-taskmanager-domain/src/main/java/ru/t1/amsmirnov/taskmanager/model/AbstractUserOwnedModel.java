package ru.t1.amsmirnov.taskmanager.model;

import org.jetbrains.annotations.NotNull;

public abstract class AbstractUserOwnedModel extends AbstractModel {

    @NotNull
    protected String userId;

    @NotNull
    public String getUserId() {
        return userId;
    }

    public void setUserId(@NotNull final String userId) {
        this.userId = userId;
    }

}
