package ru.t1.amsmirnov.taskmanager.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.model.Task;

import java.util.List;

public interface ITaskRepository {

    @Insert("INSERT INTO tm_task (id, created, name, description, status, user_id, project_id)" +
            " VALUES (#{id}, #{created}, #{name}, #{description}, #{status}, #{userId}, #{projectId})")
    void add(@NotNull Task task);

    @Nullable
    @Select("SELECT * FROM tm_task ORDER BY created")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    List<Task> findAll();

    @Nullable
    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} ORDER BY created")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    List<Task> findAllUserId(@NotNull @Param(value = "userId") String userId);

    @Nullable
    @Select("SELECT * FROM tm_task ORDER BY #{column}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    List<Task> findAllSorted(@NotNull @Param("column") String column);

    @Nullable
    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} ORDER BY #{column}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    List<Task> findAllSortedUserId(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("column") String column
    );

    @Nullable
    @Select("SELECT * FROM tm_task " +
            " WHERE user_id = #{userId} AND project_id = #{projectId}" +
            " ORDER BY created")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    List<Task> findAllByProjectId(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("projectId") String projectId
    );

    @Nullable
    @Select("SELECT * FROM tm_task " +
            " WHERE id = #{id} " +
            " ORDER BY created")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    Task findOneById(
            @NotNull @Param("id") String id
    );

    @Nullable
    @Select("SELECT * FROM tm_task " +
            " WHERE user_id = #{userId} AND id = #{id} " +
            " ORDER BY created")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    Task findOneByIdUserId(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    );

    @Select("SELECT EXISTS(SELECT 1 FROM tm_task WHERE id = #{id})")
    boolean existById(
            @NotNull @Param("id") String id
    );

    @Select("SELECT EXISTS(SELECT 1 FROM tm_task WHERE user_id = #{userId} AND id = #{id})")
    boolean existByIdUserId(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    );

    @Select("SELECT COUNT(1) FROM tm_task")
    int getSize();

    @Select("SELECT COUNT(1) FROM tm_task WHERE user_id = #{userId}")
    int getSizeUserId(@NotNull @Param("userId") String userId);

    @Update("UPDATE tm_task SET created = #{created}," +
            " name = #{name}," +
            " description = #{description}," +
            " status = #{status}," +
            " user_id = #{userId}," +
            " project_id = #{projectId}" +
            " WHERE id = #{id}")
    void update(@NotNull Task task);

    @Delete("DELETE FROM tm_task")
    void removeAll();

    @Delete("DELETE FROM tm_task WHERE user_id = #{userId}")
    void removeAllUserId(@NotNull @Param("userId") String userId);

    @Delete("DELETE FROM tm_task WHERE id = #{id}")
    void removeOneById(
            @NotNull @Param("id") String id
    );

    @Delete("DELETE FROM tm_task WHERE user_id = #{userId} AND id = #{id}")
    void removeOneByIdUserId(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    );

}
