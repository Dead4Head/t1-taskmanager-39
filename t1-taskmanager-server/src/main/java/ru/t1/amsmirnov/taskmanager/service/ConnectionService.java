package ru.t1.amsmirnov.taskmanager.service;

import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.api.repository.IProjectRepository;
import ru.t1.amsmirnov.taskmanager.api.repository.ISessionRepository;
import ru.t1.amsmirnov.taskmanager.api.repository.ITaskRepository;
import ru.t1.amsmirnov.taskmanager.api.repository.IUserRepository;
import ru.t1.amsmirnov.taskmanager.api.service.IConnectionService;
import ru.t1.amsmirnov.taskmanager.api.service.IDBProperty;

import javax.sql.DataSource;

public final class ConnectionService implements IConnectionService {

    @NotNull
    private final IDBProperty databaseProperty;

    @NotNull
    private final SqlSessionFactory sessionFactory;

    public ConnectionService(@NotNull final IDBProperty databaseProperty) {
        this.databaseProperty = databaseProperty;
        this.sessionFactory = getSqlSessionFactory();
    }

    @NotNull
    @Override
    public SqlSession getConnection() {
        return sessionFactory.openSession();
    }

    @Override
    @NotNull
    public SqlSessionFactory getSqlSessionFactory() {
        @NotNull final String username = databaseProperty.getDatabaseUser();
        @NotNull final String password = databaseProperty.getDatabasePassword();
        @NotNull final String url = databaseProperty.getDatabaseUrl();
        @NotNull final String driver = databaseProperty.getDatabaseDriver();

        final DataSource dataSource = new PooledDataSource(driver, url, username, password);
        final TransactionFactory transactionFactory = new JdbcTransactionFactory();
        final Environment environment = new Environment("development", transactionFactory, dataSource);

        final Configuration configuration = new Configuration(environment);
        configuration.addMapper(IUserRepository.class);
        configuration.addMapper(ISessionRepository.class);
        configuration.addMapper(IProjectRepository.class);
        configuration.addMapper(ITaskRepository.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }

}
