package ru.t1.amsmirnov.taskmanager.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.model.User;

import java.util.List;

public interface IUserRepository {

    @Insert("INSERT INTO tm_user (id, login, password, email, fst_name, lst_name, mid_name, role, locked)" +
            " VALUES (#{id}, #{login}, #{passwordHash}, #{email}, #{firstName}, #{lastName}, #{middleName}, #{role}, #{locked})")
    void add(@NotNull User user);

    @Nullable
    @Select("SELECT * FROM tm_user ORDER BY created")
    @Results(value = {
            @Result(property = "passwordHash", column = "password"),
            @Result(property = "firstName", column = "fst_name"),
            @Result(property = "lastName", column = "lst_name"),
            @Result(property = "middleName", column = "mid_name")
    })
    List<User> findAll();

    @Nullable
    @Select("SELECT * FROM tm_user WHERE id = #{id}")
    @Results(value = {
            @Result(property = "passwordHash", column = "password"),
            @Result(property = "firstName", column = "fst_name"),
            @Result(property = "lastName", column = "lst_name"),
            @Result(property = "middleName", column = "mid_name")
    })
    User findOneById(@NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM tm_user WHERE login = #{login}")
    @Results(value = {
            @Result(property = "passwordHash", column = "password"),
            @Result(property = "firstName", column = "fst_name"),
            @Result(property = "lastName", column = "lst_name"),
            @Result(property = "middleName", column = "mid_name")
    })
    User findOneByLogin(@NotNull @Param("login") String login);

    @Nullable
    @Select("SELECT * FROM tm_user WHERE email = #{email}")
    @Results(value = {
            @Result(property = "passwordHash", column = "password"),
            @Result(property = "firstName", column = "fst_name"),
            @Result(property = "lastName", column = "lst_name"),
            @Result(property = "middleName", column = "mid_name")
    })
    User findOneByEmail(@NotNull @Param("email") String email);

    @Select("SELECT EXISTS(SELECT 1 FROM tm_user WHERE login = #{login})")
    boolean isLoginExist(@NotNull @Param("login") String login);

    @Select("SELECT EXISTS(SELECT 1 FROM tm_user WHERE email = #{email})")
    boolean isEmailExist(@NotNull @Param("email") String email);

    @Select("SELECT COUNT(1) FROM tm_user")
    int getSize();

    @Select("SELECT EXISTS(SELECT 1 FROM tm_user WHERE id = #{id})")
    boolean existById(@NotNull @Param("id") String id);

    @NotNull
    @Update("UPDATE tm_user SET login = #{login}," +
            " password = #{passwordHash}," +
            " email = #{email}," +
            " fst_name = #{firstName}," +
            " lst_name = #{lastName}," +
            " mid_name = #{middleName}," +
            " role = #{role}," +
            " locked = #{locked}" +
            " WHERE id = #{id}")
    void update(@NotNull User user);

    @Delete("DELETE FROM tm_user WHERE id = #{id}")
    void removeOneById(
            @NotNull @Param("id") String id
    );

    @Delete("DELETE FROM tm_user")
    void removeAll();

}
