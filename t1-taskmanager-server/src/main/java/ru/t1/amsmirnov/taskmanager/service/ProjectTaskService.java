package ru.t1.amsmirnov.taskmanager.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.api.repository.IProjectRepository;
import ru.t1.amsmirnov.taskmanager.api.repository.ITaskRepository;
import ru.t1.amsmirnov.taskmanager.api.service.IConnectionService;
import ru.t1.amsmirnov.taskmanager.api.service.IProjectTaskService;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.entity.ModelNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.entity.ProjectNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.entity.TaskNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.field.ProjectIdEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.TaskIdEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.UserIdEmptyException;
import ru.t1.amsmirnov.taskmanager.model.Task;

import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IConnectionService connectionService;


    public ProjectTaskService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @Override
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final SqlSession connection = getConnection();
        try {
            @NotNull final IProjectRepository projectRepository = connection.getMapper(IProjectRepository.class);
            if (!projectRepository.existByIdUserId(userId, projectId)) throw new ProjectNotFoundException();
            @NotNull final ITaskRepository taskRepository = connection.getMapper(ITaskRepository.class);
            @Nullable final Task task = taskRepository.findOneByIdUserId(userId, taskId);
            if (task == null) throw new TaskNotFoundException();
            task.setProjectId(projectId);
            taskRepository.update(task);
            connection.commit();
        } catch (final Exception exception) {
            connection.rollback();
            throw exception;
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @NotNull final SqlSession connection = getConnection();
        try {
            @NotNull final IProjectRepository projectRepository = connection.getMapper(IProjectRepository.class);
            if (!projectRepository.existByIdUserId(userId, projectId)) throw new ProjectNotFoundException();
            @NotNull final ITaskRepository taskRepository = connection.getMapper(ITaskRepository.class);
            @Nullable final List<Task> tasks = taskRepository.findAllByProjectId(userId, projectId);
            if (tasks == null) throw new ModelNotFoundException("TaskList");
            for (final Task task : tasks)
                taskRepository.removeOneByIdUserId(userId, task.getId());
            projectRepository.removeOneByIdUserId(userId, projectId);
            connection.commit();
        } catch (final Exception exception) {
            connection.rollback();
            throw exception;
        } finally {
            connection.close();
        }
    }

    @Override
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final SqlSession connection = getConnection();
        try {
            @NotNull final IProjectRepository projectRepository = connection.getMapper(IProjectRepository.class);
            if (!projectRepository.existByIdUserId(userId, projectId)) throw new ProjectNotFoundException();
            @NotNull final ITaskRepository taskRepository = connection.getMapper(ITaskRepository.class);
            @Nullable final Task task = taskRepository.findOneByIdUserId(userId, taskId);
            if (task == null) throw new TaskNotFoundException();
            task.setProjectId(null);
            taskRepository.update(task);
            connection.commit();
        } catch (final Exception exception) {
            connection.rollback();
            throw exception;
        } finally {
            connection.close();
        }
    }

    @NotNull
    public SqlSession getConnection() {
        return connectionService.getConnection();
    }

}
