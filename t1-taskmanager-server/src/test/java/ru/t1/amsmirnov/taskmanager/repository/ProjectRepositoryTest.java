package ru.t1.amsmirnov.taskmanager.repository;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.amsmirnov.taskmanager.api.repository.IProjectRepository;
import ru.t1.amsmirnov.taskmanager.api.service.IConnectionService;
import ru.t1.amsmirnov.taskmanager.enumerated.ProjectSort;
import ru.t1.amsmirnov.taskmanager.marker.DBCategory;
import ru.t1.amsmirnov.taskmanager.model.Project;
import ru.t1.amsmirnov.taskmanager.service.ConnectionService;
import ru.t1.amsmirnov.taskmanager.service.PropertyService;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

@Category(DBCategory.class)
public class ProjectRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    private static final String NONE_ID = "---NONE---";

    @NotNull
    private static final String USER_ALFA_ID = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_BETA_ID = UUID.randomUUID().toString();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(new PropertyService());

    @NotNull
    private SqlSession connection;

    @NotNull
    private List<Project> projects;

    @NotNull
    private List<Project> alfaProjects;

    @NotNull
    private List<Project> betaProjects;

    @NotNull
    private IProjectRepository projectRepository;

    @Before
    public void initRepository() {
        connection = CONNECTION_SERVICE.getConnection();
        projectRepository = connection.getMapper(IProjectRepository.class);
        projects = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = new Project();
            project.setName("Project " + (NUMBER_OF_ENTRIES - i));
            project.setDescription("Description " + i);
            if (i < 5)
                project.setUserId(USER_ALFA_ID);
            else
                project.setUserId(USER_BETA_ID);
            try {
                projectRepository.add(project);
                projects.add(project);
            } catch (final Exception e) {
                connection.rollback();
                throw e;
            }
        }
        projects = projects.stream()
                .sorted(ProjectSort.BY_CREATED.getComparator())
                .collect(Collectors.toList());
        alfaProjects = projects
                .stream()
                .filter(p -> p.getUserId().equals(USER_ALFA_ID))
                .collect(Collectors.toList());
        betaProjects = projects
                .stream()
                .filter(p -> p.getUserId().equals(USER_BETA_ID))
                .collect(Collectors.toList());
    }

    @After
    public void clearRepository() {
        try {
            projectRepository.removeAll();
        } catch (final Exception e) {
            connection.rollback();
            throw e;
        }
        projects.clear();
    }

    @Test
    public void testAdd() {
        @NotNull final Project project = new Project();
        project.setName("Added project");
        project.setDescription("Added description");
        project.setUserId(USER_ALFA_ID);
        try {
            projectRepository.add(project);
            projects.add(project);
            @Nullable final List<Project> actualProjectList = projectRepository.findAll();
            assertEquals(projects, actualProjectList);
        } catch (final Exception e) {
            connection.close();
            throw e;
        }
    }

    @Test
    public void testRemoveAll() {
        try {
            assertNotEquals(0, projectRepository.getSize());
            projectRepository.removeAll();
            assertEquals(0, projectRepository.getSize());
        } catch (final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    public void testRemoveAllForUser() {
        try {
            assertNotEquals(0, projectRepository.getSizeUserId(USER_ALFA_ID));
            projectRepository.removeAllUserId(USER_ALFA_ID);
            assertEquals(0, projectRepository.getSizeUserId(USER_ALFA_ID));
        } catch (final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    public void testExistById() {
        try {
            for (final Project project : projects) {
                assertTrue(projectRepository.existByIdUserId(project.getUserId(), project.getId()));
            }
            assertFalse(projectRepository.existByIdUserId(USER_ALFA_ID, NONE_ID));
        } catch (final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    public void testFindAll() {
        try {
            List<Project> actualAlfaProjects = projectRepository.findAllUserId(USER_ALFA_ID);
            List<Project> actualBetaProjects = projectRepository.findAllUserId(USER_BETA_ID);
            assertNotEquals(actualAlfaProjects, actualBetaProjects);
            assertEquals(alfaProjects, actualAlfaProjects);
            assertEquals(betaProjects, actualBetaProjects);
        } catch (final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    @Ignore
    public void testFindAllComparator() {
        try {
            @NotNull final Comparator<Project> comparator = ProjectSort.BY_NAME.getComparator();
            List<Project> actualAlfaProjects = projectRepository.findAllSortedUserId(USER_ALFA_ID, "name");
            alfaProjects = alfaProjects.stream().sorted(comparator).collect(Collectors.toList());
            assertEquals(alfaProjects, actualAlfaProjects);
        } catch (final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    public void testFindOneById() {
        try {
            for (final Project project : projects) {
                assertEquals(project, projectRepository.findOneByIdUserId(project.getUserId(), project.getId()));
            }
            assertNull(projectRepository.findOneByIdUserId(USER_ALFA_ID, NONE_ID));
        } catch (final Exception e) {
            connection.rollback();
            throw e;
        }
    }


    @Test
    public void testGetSize() {
        try {
            assertEquals(projects.size(), projectRepository.getSize());
            assertEquals(alfaProjects.size(), projectRepository.getSizeUserId(USER_ALFA_ID));
            assertEquals(betaProjects.size(), projectRepository.getSizeUserId(USER_BETA_ID));
        } catch (final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    public void testRemoveOneById() {
        try {
            for (final Project project : projects) {
                assertTrue(projectRepository.existById(project.getId()));
                projectRepository.removeOneByIdUserId(project.getUserId(), project.getId());
                assertFalse(projectRepository.existById(project.getId()));
            }
        } catch (final Exception e) {
            connection.rollback();
            throw e;
        }
    }

}
