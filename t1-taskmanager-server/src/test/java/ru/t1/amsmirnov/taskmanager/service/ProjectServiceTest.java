package ru.t1.amsmirnov.taskmanager.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.amsmirnov.taskmanager.api.repository.IProjectRepository;
import ru.t1.amsmirnov.taskmanager.api.service.IConnectionService;
import ru.t1.amsmirnov.taskmanager.api.service.IProjectService;
import ru.t1.amsmirnov.taskmanager.enumerated.ProjectSort;
import ru.t1.amsmirnov.taskmanager.enumerated.Status;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.field.IdEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.NameEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.UserIdEmptyException;
import ru.t1.amsmirnov.taskmanager.marker.DBCategory;
import ru.t1.amsmirnov.taskmanager.model.Project;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

@Category(DBCategory.class)
public class ProjectServiceTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static final String NONE_STR = "---NONE---";

    @Nullable
    private static final String NULL_STR = null;

    @NotNull
    private static final String USER_ALFA_ID = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_BETA_ID = UUID.randomUUID().toString();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(new PropertyService());

    @NotNull
    private IProjectRepository projectRepository;

    @NotNull
    private IProjectService projectService;

    @NotNull
    private final List<Project> projects = new ArrayList<>();

    @NotNull
    private List<Project> alfaProjects;

    @NotNull
    private List<Project> betaProjects;

    @Before
    public void initRepository() throws Exception {
        SqlSession session = CONNECTION_SERVICE.getConnection();
        projectRepository = session.getMapper(IProjectRepository.class);
        projectService = new ProjectService(CONNECTION_SERVICE);
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            Thread.sleep(2);
            @NotNull final Project project = new Project();
            project.setName("Project " + i);
            project.setDescription("Description " + i);
            if (i <= 5)
                project.setUserId(USER_ALFA_ID);
            else
                project.setUserId(USER_BETA_ID);
            projectRepository.add(project);
            session.commit();
            projects.add(project);
        }
        session.close();
        alfaProjects = projects
                .stream()
                .filter(p -> p.getUserId().equals(USER_ALFA_ID))
                .sorted(ProjectSort.BY_CREATED.getComparator())
                .collect(Collectors.toList());
        betaProjects = projects
                .stream()
                .filter(p -> p.getUserId().equals(USER_BETA_ID))
                .sorted(ProjectSort.BY_CREATED.getComparator())
                .collect(Collectors.toList());
    }

    @After
    public void clearRepository() {
        projectService.removeAll();
    }

    @Test
    public void testAdd() throws AbstractException {
        @NotNull final Project project = new Project();
        project.setName("Test project");
        project.setDescription("Test description");
        project.setUserId(USER_ALFA_ID);

        projects.add(project);
        projectService.add(USER_ALFA_ID, project);
        List<Project> actualProjectList = projectService.findAll();
        assertEquals(projects, actualProjectList);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testAdd_EmptyUserException_1() throws AbstractException {
        projectService.add(NULL_STR, new Project());
    }

    @Test(expected = UserIdEmptyException.class)
    public void testAdd_EmptyUserException_2() throws AbstractException {
        projectService.add("", new Project());
    }

    @Test
    public void testRemoveAll() throws AbstractException {
        assertNotEquals(0, projectService.findAll(USER_ALFA_ID).size());
        projectService.removeAll(USER_ALFA_ID);
        assertEquals(0, projectService.findAll(USER_ALFA_ID).size());
    }

    @Test(expected = UserIdEmptyException.class)
    public void testRemoveAll_EmptyUserException_1() throws AbstractException {
        projectService.removeAll(NULL_STR);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testRemoveAll_EmptyUserException_2() throws AbstractException {
        projectService.removeAll("");
    }

    @Test
    public void testFindAll() throws AbstractException {
        final List<Project> alfaServProjects = projectService.findAll(USER_ALFA_ID);
        final List<Project> betaServProjects = projectService.findAll(USER_BETA_ID, ProjectSort.BY_CREATED.getComparator());
        final List<Project> actualProjectsAll = projectService.findAll();
        final List<Project> actualProjectsComp = projectService.findAll(ProjectSort.BY_CREATED.getComparator());

        assertNotEquals(alfaServProjects, betaServProjects);
        assertEquals(alfaProjects, alfaServProjects);
        assertEquals(betaProjects, betaServProjects);
        assertEquals(projects, actualProjectsAll);
        assertEquals(projects, actualProjectsComp);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindAll_EmptyUserException_1() throws AbstractException {
        projectService.findAll(NULL_STR);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindAll_EmptyUserException_2() throws AbstractException {
        projectService.findAll("");
    }

    @Test
    public void testFindOneById() throws AbstractException {
        for (final Project project : projects) {
            assertEquals(project, projectService.findOneById(project.getUserId(), project.getId()));
        }
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindOneById_EmptyUserException_1() throws AbstractException {
        projectService.findOneById(NULL_STR, NONE_STR);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindOneById_EmptyUserException_2() throws AbstractException {
        projectService.findOneById("", NONE_STR);
    }

    @Test(expected = IdEmptyException.class)
    public void testFindOneById_EmptyIdException_1() throws AbstractException {
        projectService.findOneById(NONE_STR, NULL_STR);
    }

    @Test(expected = IdEmptyException.class)
    public void testFindOneById_EmptyIdException_2() throws AbstractException {
        projectService.findOneById(NONE_STR, "");
    }

    @Test
    public void testRemoveOne() throws AbstractException {
        for (final Project project : projects) {
            assertTrue(projectService.existById(project.getId()));
            projectService.removeOne(project.getUserId(), project);
            assertFalse(projectService.existById(project.getId()));
        }
    }

    @Test(expected = UserIdEmptyException.class)
    public void testRemove_EmptyUserException_1() throws AbstractException {
        projectService.removeOne("", new Project());
    }

    @Test(expected = UserIdEmptyException.class)
    public void testRemove_EmptyUserException_2() throws AbstractException {
        projectService.removeOne(NULL_STR, new Project());
    }

    @Test
    public void testRemoveById() throws AbstractException {
        for (final Project project : projects) {
            assertTrue(projectService.existById(project.getId()));
            projectService.removeOneById(project.getUserId(), project.getId());
            assertFalse(projectService.existById(project.getId()));
        }
    }

    @Test(expected = UserIdEmptyException.class)
    public void testRemoveById_EmptyUserException_1() throws AbstractException {
        projectService.removeOneById("", NONE_STR);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testRemoveById_EmptyUserException_2() throws AbstractException {
        projectService.removeOneById(NULL_STR, NONE_STR);
    }

    @Test(expected = IdEmptyException.class)
    public void testRemoveById_EmptyIdException_1() throws AbstractException {
        projectService.removeOneById(NONE_STR, NULL_STR);
    }

    @Test(expected = IdEmptyException.class)
    public void testRemoveById_EmptyIdException_2() throws AbstractException {
        projectService.removeOneById(NONE_STR, "");
    }

    @Test
    public void testExistById() throws AbstractException {
        for (final Project project : projects) {
            assertTrue(projectService.existById(project.getUserId(), project.getId()));
        }
        assertFalse(projectService.existById(USER_ALFA_ID, NONE_STR));
    }

    @Test(expected = UserIdEmptyException.class)
    public void testExistById_UserEmptyException_1() throws AbstractException {
        projectService.existById(NULL_STR, NONE_STR);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testExistById_UserEmptyException_2() throws AbstractException {
        projectService.existById("", NONE_STR);
    }

    @Test(expected = IdEmptyException.class)
    public void testExistById_EmptyIdException_1() throws AbstractException {
        projectService.existById(NONE_STR, NULL_STR);
    }

    @Test(expected = IdEmptyException.class)
    public void testExistById_EmptyIdException_2() throws AbstractException {
        projectService.existById(NONE_STR, "");
    }

    @Test
    public void testGetSize() throws AbstractException {
        assertEquals(alfaProjects.size(), projectService.getSize(USER_ALFA_ID));
        assertEquals(betaProjects.size(), projectService.getSize(USER_BETA_ID));
    }

    @Test(expected = UserIdEmptyException.class)
    public void testGetSize_EmptyUserException_1() throws AbstractException {
        projectService.getSize(NULL_STR);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testGetSize_EmptyUserException_2() throws AbstractException {
        projectService.getSize("");
    }

    @Test
    public void testChangeProjectStatusById() throws AbstractException {
        int i = 0;
        for (final Project project : projects) {
            if (i % 2 == 0) {
                projectService.changeStatusById(project.getUserId(), project.getId(), Status.IN_PROGRESS);
                assertEquals(Status.IN_PROGRESS, projectService.findOneById(project.getId()).getStatus());
                projectService.changeStatusById(project.getUserId(), project.getId(), null);
                assertEquals(Status.IN_PROGRESS, projectService.findOneById(project.getId()).getStatus());
            } else {
                projectService.changeStatusById(project.getUserId(), project.getId(), Status.COMPLETED);
                assertEquals(Status.COMPLETED, projectService.findOneById(project.getId()).getStatus());
            }
            i++;
        }
    }

    @Test(expected = IdEmptyException.class)
    public void testChangeProjectStatusById_EmptyIdException_1() throws AbstractException {
        projectService.changeStatusById(NONE_STR, NULL_STR, Status.IN_PROGRESS);
    }

    @Test(expected = IdEmptyException.class)
    public void testChangeProjectStatusById_EmptyIdException_2() throws AbstractException {
        projectService.changeStatusById(NONE_STR, "", Status.IN_PROGRESS);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testChangeProjectStatusById_EmptyUserException_1() throws AbstractException {
        projectService.changeStatusById("", NONE_STR, Status.IN_PROGRESS);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testChangeProjectStatusById_EmptyUserException_2() throws AbstractException {
        projectService.changeStatusById(NULL_STR, NONE_STR, Status.IN_PROGRESS);
    }

    @Test
    public void testCreate() throws AbstractException {
        final String name = "Project Test";
        final String description = "Description";
        final Project project = projectService.create(USER_ALFA_ID, name, description);
        final Project actualProject = projectService.findOneById(project.getId());
        assertEquals(name, actualProject.getName());
        assertEquals(description, actualProject.getDescription());
        assertEquals(USER_ALFA_ID, actualProject.getUserId());
        assertEquals(projectService.getSize(), projects.size() + 1);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testCreate_EmptyUserException_1() throws AbstractException {
        final String name = "Project Test";
        final String description = "Description";
        projectService.create(NULL_STR, name, description);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testCreate_EmptyUserException_2() throws AbstractException {
        final String name = "Project Test";
        final String description = "Description";
        projectService.create("", name, description);
    }

    @Test(expected = NameEmptyException.class)
    public void testCreate_EmptyNameException_1() throws AbstractException {
        final String description = "Description";
        projectService.create(NONE_STR, NULL_STR, description);
    }

    @Test(expected = NameEmptyException.class)
    public void testCreate_EmptyNameException_2() throws AbstractException {
        final String description = "Description";
        projectService.create(NONE_STR, "", description);
    }

    @Test
    public void testUpdateById() throws AbstractException {
        for (final Project project : projects) {
            final String name = project.getName() + "TEST";
            final String description = project.getDescription() + "TEST";
            project.setDescription(description);
            project.setName(name);
            projectService.updateById(project.getUserId(), project.getId(), name, description);
            assertEquals(project.getName(), name);
            assertEquals(project.getDescription(), description);
        }
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateById_NameEmptyException_1() throws AbstractException {
        projectService.updateById(NONE_STR, NONE_STR, "", "");
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateById_NameEmptyException_2() throws AbstractException {
        projectService.updateById(NONE_STR, NONE_STR, NULL_STR, "");
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUpdateById_UserEmptyException_1() throws AbstractException {
        projectService.updateById(NULL_STR, NONE_STR, "Name", "Desc");
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUpdateById_UserEmptyException_2() throws AbstractException {
        projectService.updateById("", NONE_STR, "Name", "Desc");
    }

    @Test(expected = IdEmptyException.class)
    public void testUpdateById_IdEmptyException_1() throws AbstractException {
        projectService.updateById(NONE_STR, NULL_STR, "Name", "Desc");
    }

    @Test(expected = IdEmptyException.class)
    public void testUpdateById_IdEmptyException_2() throws AbstractException {
        projectService.updateById(NONE_STR, "", "Name", "Desc");
    }

}
