package ru.t1.amsmirnov.taskmanager.repository;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.amsmirnov.taskmanager.api.repository.ISessionRepository;
import ru.t1.amsmirnov.taskmanager.api.service.IConnectionService;
import ru.t1.amsmirnov.taskmanager.marker.DBCategory;
import ru.t1.amsmirnov.taskmanager.model.Session;
import ru.t1.amsmirnov.taskmanager.service.ConnectionService;
import ru.t1.amsmirnov.taskmanager.service.PropertyService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

@Category(DBCategory.class)
public class SessionRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static final String NONE_ID = "---NONE---";

    @NotNull
    private static final String USER_ALFA_ID = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_BETA_ID = UUID.randomUUID().toString();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(new PropertyService());

    @NotNull
    private static final Date TODAY = new Date();

    @NotNull
    private final List<Session> sessions = new ArrayList<>();

    private ISessionRepository sessionRepository;

    private SqlSession connection;

    @Before
    public void initRepository() throws Exception {
        connection = CONNECTION_SERVICE.getConnection();
        sessionRepository = connection.getMapper(ISessionRepository.class);
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            Thread.sleep(2);
            @NotNull final Session session = new Session();
            session.setCreated(TODAY);
            if (i <= 5)
                session.setUserId(USER_ALFA_ID);
            else
                session.setUserId(USER_BETA_ID);
            try {
                sessions.add(session);
                sessionRepository.add(session);
            } catch (final Exception e) {
                connection.rollback();
                throw e;
            }
        }
    }

    @After
    public void clearRepository() throws Exception {
        try {
            sessionRepository.removeAll();
            sessions.clear();
        } catch (final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    public void removeOneByIdTest() throws Exception {
        try {
            for (Session session : sessions) {
                assertNotNull(sessionRepository.findOneById(session.getId()));
                sessionRepository.removeOneById(session.getId());
                assertNull(sessionRepository.findOneById(session.getId()));
            }
            sessions.clear();
            assertEquals(sessions, sessionRepository.findAll());
        } catch (final Exception e) {
            connection.rollback();
            throw e;
        }
    }

}
